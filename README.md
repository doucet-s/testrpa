# AUTOP

Playbook Ansible pour le déploiement des postes robots RPA du projet AUTOP

## Prérequis
### VMware
- Un modèle de machine virtuel VMware en windows 10 avec le service WinRM d'activer
- Le nom d'un compte avec les privilèges d'administration et son mot de passe
- la liste des comptes AD de Bercy autorisés à se connecter en RDP sur le poste RPA

### IPAM SolidSever
- Un compte et mot de passe pour utiliser l'API SolidRestServer
- Les droits d'ajouts, modifications et suppressions sur le sous-réseau 172.19.69.0/24 DMZ-RPA-AUTOP vlan 269

## Utilisation

Pour utiliser ce playbook directement depuis ce serveur GIT, il faut créer un fichier de tache ansible avec les actions suivntes:
 - création d'un dossier temporaire 
 - récupération des sources depuis vvd-gitlab
 - exécution du playbook
 - suppression du dossier temporaire

exemple: création du fichier **gestion_vpar.yml**:
```python
---
  - name: Socle sur Robot VPA
    hosts: localhost
    gather_facts: no
    connection: local
    vars_files:
       - /root/.nubo_secrets.yml
    vars:
      - branche: 'main'
    tasks:
       - name: Create temporary directory
         ansible.builtin.tempfile:
           state: directory
           suffix: .autop
         register: autop_dir

       - name: Download the autop code from the GitRepo
         become: yes
         git:
            repo: 'https://vvd-gitlab.alize/NUBO/autop.git'
            dest: "{{ autop_dir.path }}/autop"
            version: "{{ branche | default('main') }}"

       - name: Run autop role
          shell: export ANSIBLE_LOG_PATH={{ autop_dir.path }}/ansible.log && /applis/python-3.6/bin/ansible-playbook  {{autop_dir.path}}/autop/manage_vpar.yml  -e "{{ playbook_args }} log_dir={{ autop_dir.path }}"

       - name: Remove temp directory
         ansible.builtin.file:
           path: "{{ autop_dir.path }}"
           state: absent
         when: autop_dir.path is defined
```

### Création du poste
Pour créer un poste depuis Nubo:         
```bash
/applis/python-3.6/bin/ansible-playbook  gestion_vpar.yml  -e "playbook_args='vm_service=BITS vm_type=P vm_num_cpus=2 vm_memory_mb=4096 rds_users=sdoucet-adc vm_action=create'"
```
Cette commande crée un poste VPAR de Production (vm_type=P) pour le service BITS.

Ce poste sera créé dans le dossier datacenter/vm/PDT-Robot/BITS/Production

Le nom du poste sera BITS-VPAR-Pxx où xx représente un nombre entre 1 et 99. le premier nombre disponible est utilisé si un autre poste BITS-VPAR-Pxx existe dans le vcenter.

L'adresse IP sera automatiquement réservé dans l'IPAM de recette sur le vlan 269.

L'utilisateur sdoucet-adc sera ajouté au groupe 'Utilisateurs du bureau à distance' du poste.


### Suppression du poste
Pour supprimer un poste VPAR, il faut executer la commande ci dessous depuis Nubo:         
```bash
/applis/python-3.6/bin/ansible-playbook  gestion_vpar.yml  -e  "playbook_args='vm_name=BITS-VPAR-R03 vm_action=delete'"
```

Le poste sera arrêté puis supprimé et l'adresse IP dans l'IPAM de recette sera libérée.

